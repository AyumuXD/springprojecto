import clases.Rectangulo

fun main() {
    val rectangulo = Rectangulo(45.0,4.0);

    println(rectangulo.obtenerArea())
    println(rectangulo.obtenerPerimetros())

    println()
    println(rectangulo.obtenerAreaPlg())
    println(rectangulo.obtenerPerimetrosPlg())

    println(rectangulo.dibujar())

}