package clases

abstract class Poligono {
    abstract fun obtenerArea():Double
    abstract fun obtenerPerimetros() :Double
    abstract fun dibujar()

    protected fun centimetrosAPulgadas(valor:Double):Double{
        return (1 / 2.54) * valor;
    }
}