package clases

import kotlin.math.roundToInt

class Rectangulo (var base:Double, var altura:Double): IPoligono {
    override fun obtenerArea(): Double {
        return base * altura;
    }

    override fun obtenerPerimetros(): Double {
        return (2 * altura) + (2 * base);
    }

    override fun dibujar() {
        for (i in 1..altura.roundToInt()){
            println(" * ".repeat(base.toInt()))
        }
    }


    fun obtenerAreaPlg(): Double {
        return centimetrosAPulgadas(obtenerArea());
    }

    fun obtenerPerimetrosPlg(): Double {
        return centimetrosAPulgadas(obtenerPerimetros())
    }

}