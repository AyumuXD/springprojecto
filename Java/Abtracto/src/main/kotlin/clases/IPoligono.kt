package clases

interface IPoligono {
    fun obtenerArea():Double
    fun obtenerPerimetros() :Double
    fun dibujar()

    fun centimetrosAPulgadas(valor:Double):Double{
        return (1 / 2.54) * valor;
    }
}