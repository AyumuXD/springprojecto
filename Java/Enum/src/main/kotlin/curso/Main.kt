package curso

fun main() {
    val listaTareas = arrayListOf(Tarea("Lava ropa",EstadoTarea.PENDIENTE), Tarea("Compra medicamentos",EstadoTarea.PENDIENTE))

    var tareaPendientes = 0;

    listaTareas.forEach {
        if (it.estado == EstadoTarea.PENDIENTE) tareaPendientes++
    }

    println("Tareas pendientes: $tareaPendientes")
}

class Tarea(var nombre:String,var estado:EstadoTarea){

}

enum class EstadoTarea{
    PENDIENTE,
    EN_PROCESO,
    REALIZADA
}