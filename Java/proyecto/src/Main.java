import java.nio.charset.StandardCharsets;

public class Main {
    public static void main(String[] args) {
        boolean bandera = true;


        if (bandera){ //Solo sucera si el caso es verdadero
            System.out.println("Verdaderos");
        }else {  ///Solo sucede si el caso es contrario
            System.out.println("Falso");
        }


        ///<, >  <= =>  == !=
        System.out.println("<");
        int edad = 18;
        if(edad < 18){
            System.out.println("Es menor de edad");
        }else{
            System.out.println("Es mayor de edad");
        }

        System.out.println(">");
        edad = 18;
        if(edad > 18){
            System.out.println("Es mayor de edad");
        }else{
            System.out.println("Es menor de edad");
        }

        System.out.println(">=");
        edad = 18;
        if(edad >= 18){
            System.out.println("Es mayor de edad");
        }else{
            System.out.println("Es menor de edad");
        }

        System.out.println("<=");
        edad = 18;
        if(edad <= 18){
            System.out.println("Es menor de edad");
        }else{
            System.out.println("Es mayor de edad");
        }

        System.out.println("==");
        if(18 == 15){
            System.out.println("Es igual");
        }else {
            System.out.println("No es igual");
        }


        bandera = false;

        System.out.println("!= diferente");
        if (bandera != true){ //Solo sucera si el caso es verdadero
            System.out.println("Verdaderos");
        }else {  ///Solo sucede si el caso es contrario
            System.out.println("Falso");
        }


        bandera = true;

        System.out.println("! not o lo convierte en valor contrario");
        if (!bandera){ //Solo sucera si el caso es verdadero
            System.out.println("Verdaderos");
        }else {  ///Solo sucede si el caso es contrario
            System.out.println("Falso");
        }

        String color = "plateado";
        ///Cascada o anidada
        ///Jijo de puta
        if(color == "azul"){
            System.out.println("color azul");
        } else if (color == "rojo")  { // sino si(condicion)
            System.out.println("color rojo");
        } else if (color == "plateado") { // sino si(condicion)
            System.out.println("color plateado");
        }


        color = "cualquiera";
        System.out.println("multidecisiones");
        switch (color){
            case "azul":
                System.out.println("color azul");
                break; ///romper
            case "rojo":
                System.out.println("color rojo");
                break;
            case "plateado":
                System.out.println("color plateado");
                break;
            default:
                System.out.println("Color cualquiera");
                break;
        }


    }
}