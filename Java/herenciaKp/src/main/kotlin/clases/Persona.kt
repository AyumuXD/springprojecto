package clases

import java.time.LocalDate
import java.time.Period
import java.time.format.DateTimeFormatter

open class Persona(
    var nombre: String,
    var apellido: String,
    var direccion: String,
    var telefono: String?,
    var fechaNacimientoTexto: String
) {
    var fechaNacimientoDate:LocalDate = LocalDate.parse(fechaNacimientoTexto, DateTimeFormatter.ofPattern(FORMATO_FECHA))

    fun obtenerEdad():Int{
        return Period.between(fechaNacimientoDate,LocalDate.now()).years;
    }

    override fun toString(): String {
        return "Nombre: $nombre $apellido, Direccion: $direccion, telefono: ${telefono ?: "N/D"}, Fecha de nacimiento: $fechaNacimientoDate, Edad: ${obtenerEdad()}"
    }


    companion object{
        const val FORMATO_FECHA = "dd/MM/yyyy"
    }
}