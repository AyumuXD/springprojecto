package clases

class Administrador (
    nombre: String,
    apellido: String,
    direccion: String,
    telefono: String?,
    fechaNacimientoTexto: String,
    salario:Double,
    var porcentajeBoono:Double
) : Empleado(nombre,apellido,direccion,telefono,fechaNacimientoTexto,salario) {

    fun obtenerSalario():Double{
        return salario + ((100 / salario) *  porcentajeBoono)
    }

    override fun toString(): String {
        return super.toString() + ", Porcentaje bono ${porcentajeBoono} y salario final: ${obtenerSalario()}"
    }
}