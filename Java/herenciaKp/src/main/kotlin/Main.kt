import clases.Administrador
import clases.Empleado
import clases.Persona

fun main() {

    val persona = Persona("Jesus2", "Moreno2","Conocida2","57872574","25/01/1995")

    val empleado = Empleado("Jesus", "Moreno","Conocida",null,"25/01/1990",585.12)

    val admin = Administrador("Jesus2", "Moreno2","Conocida2","57872574","25/01/1995",100.0,10.0);

    println(persona)

    println(empleado)

    println(admin)
}